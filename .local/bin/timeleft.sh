#!/usr/bin/env sh

job=$(oarstat -u -J)
if [ -n "$job" ]; then
	DATE_NOW=$(date +%s)
	DATE_END=$(echo "$job" | jq '.[] | .walltime + .scheduled_start')
	LEFT=$(( (DATE_END - DATE_NOW)/60 ))
	echo $LEFT min left
else
	echo no job
fi
